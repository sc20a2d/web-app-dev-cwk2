import os
import unittest

import flask_login
from flask import Flask
from flask_login import login_user, LoginManager
from flask_sqlalchemy import SQLAlchemy
from app import app, db, models, login_manager
from app.models import Account

login_manager = LoginManager()
login_manager.init_app(app)


def login(client, username, password):
    return client.post('/login', data=dict(
        username=username,
        password=password
    ), follow_redirects=True)


def logout(client):
    return client.get('/logout', follow_redirects=True)


class TestCase(unittest.TestCase):
    def setUp(self):
        app.config.from_object('config')
        app.config['TESTING'] = True
        app.config['LOGIN_DISABLED'] = True
        app.config['WTF_CSRF_ENABLED'] = False
        # the basedir lines could be added like the original db
        app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///test.db'
        self.app = app.test_client()
        db.create_all()
        app.config['LOGIN_DISABLED'] = False

        app.testing = True
        client = app.test_client()
        pass

    def tearDown(self):
        db.session.remove()
        db.drop_all()

    def test_user_db(self):
        # set up test user
        test_user = models.Account(username="TestUser",
                                   password="TestPassword")
        db.session.add(test_user)
        db.session.commit()
        user = Account.query.filter_by(username="TestUser", password="TestPassword").first()
        self.assertEqual(test_user.username, user.username)

    # Should pass
    def test_index_route(self):
        response = self.app.get('/',
                                follow_redirects=True)
        self.assertEqual(response.status_code, 200)

    def test_create_route(self):
        response = self.app.get('/create',
                                follow_redirects=True)
        self.assertEqual(response.status_code, 200)

    def test_login_route(self):
        response = self.app.get('/login',
                                follow_redirects=True)
        self.assertEqual(response.status_code, 200)

    # Should fail, user not logged in
    def test_profile_route(self):
        response = self.app.get('/profile',
                                follow_redirects=True)
        self.assertEqual(response.status_code, 401)

    def test_review_route(self):
        response = self.app.get('/review',
                                follow_redirects=True)
        self.assertEqual(response.status_code, 401)


@login_manager.user_loader
def load_user(id):
    return Account.query.get(int(id))


if __name__ == '__main__':
    unittest.main()

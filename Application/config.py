import os

WTF_CSRF_ENABLED = True
SECRET_KEY = '7G2vOJs183xnLGl5dF9y'

basedir = os.path.abspath(os.path.dirname(__file__))
SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(basedir, 'app.db')
SQLALCHEMY_TRACK_MODIFICATIONS = True

import logging
from flask import render_template, request, redirect, flash, url_for, abort
from flask_admin.helpers import is_safe_url
from flask_login import login_user, current_user, login_required, logout_user

from app import app, db, admin, models, login_manager
from app.models import Review, Account, Book
from flask_admin.contrib.sqla import ModelView
from .forms import CreateAccount, CreateReview, ChangePassword, LogIn


admin.add_view(ModelView(Review, db.session))
admin.add_view(ModelView(Account, db.session))
admin.add_view(ModelView(Book, db.session))


@login_manager.user_loader
def load_user(user_id):
    logging.debug('load_user called')
    return Account.query.get(int(user_id))


@app.route('/')
def index():
    app.logger.info('index route request')
    reviews = models.Review.query.all()

    return render_template('index.html', reviews=reviews)


@app.route('/create', methods=['GET', 'POST'])
def create():
    app.logger.info('create route request')
    form = CreateAccount(request.form)
    if request.method == 'POST':
        form_entry = models.Account(username=form.username.data,
                                    password=form.password.data,
                                    date_of_birth=form.date_of_birth.data)

        # Check if user exists
        user = Account.query.filter_by(username=form.username.data).first()

        # Username exists in database
        if user is not None:
            logging.warning('Username already exists!')
            flash("Username already exists!")
            return render_template('create.html', Form=form)

        logging.info('adding ' + form.username.data + 'to database')
        db.session.add(form_entry)
        db.session.commit()
        logging.info('added ' + form.username.data + 'to database')
        user = Account.query.filter_by(username=form.username.data).first()
        login_user(user)

        return redirect('/')

    return render_template('create.html', Form=form)


@app.route('/login', methods=['GET', 'POST'])
def login():
    app.logger.info('login route request')
    form = LogIn(request.form)
    if request.method == 'POST':
        form_entry = models.Account(username=form.username.data,
                                    password=form.password.data)

        temp_user = Account.query.filter_by(username=form_entry.username,
                                            password=form_entry.password).first()

        # Username and password exist in db and are the same user
        if temp_user is not None:
            if form.stayLoggedIn:
                logging.info('Remember: Logging in user: ' + form.username.data)
                login_user(temp_user, remember=True)
                logging.info(form.username.data + ' logged in')
            else:
                logging.info('Don\'t Remember: Logging in user: ' + form.username.data)
                login_user(temp_user, remember=False)
                logging.info(form.username.data + ' logged in')
            return redirect('/')

    return render_template('login.html', Form=form)


@app.route('/logout')
def logout():
    app.logger.info('logout route request')
    logging.info(current_user.username + ' logged out')
    logout_user()
    return redirect('/')


@app.route('/profile', methods=['GET', 'POST'])
@login_required
def profile():
    app.logger.info('profile route request')
    temp_user = Account.query.filter_by(username=current_user.username).first()
    profile_review = Review.query.filter_by(account_id=current_user.id).all()

    form = ChangePassword(request.form)
    if request.method == 'POST':

        if current_user.password == form.old_password.data:
            temp_user = Account.query.filter_by(username=current_user.username,
                                                password=form.old_password.data).first()
            temp_user.password = form.password.data
            logging.info('changing ' + current_user.username + '\'s password')
            db.session.commit()
            logging.info(current_user.username + '\'s password changed')
            return redirect('/')
        flash("Old password does not match the one in database.")
    return render_template('profile.html', Form=form, profile_review=profile_review)


@app.route('/delete/<int:review_id>')
def delete(review_id):
    app.logger.info('delete review route request')
    review_to_delete = models.Review.query.get_or_404(review_id)
    db.session.delete(review_to_delete)
    logging.info('Deleting review id: ' + review_to_delete.id + ' from database')
    temp_id = review_to_delete.id
    db.session.commit()
    logging.info('Deleted review id: ' + temp_id + ' from database')
    return redirect('/profile')


@app.route('/review', methods=['GET', 'POST'])
@login_required
def review():
    app.logger.info('review route request')
    form = CreateReview(request.form)
    form.book_title.choices = [(book.id, book.name) for book in Book.query.all()]
    if request.method == 'POST':
        temp_user = Account.query.filter_by(username=current_user.username).first()
        form_entry = models.Review(rating=form.rating.data,
                                   content=form.review_content.data,
                                   book_id=form.book_title.data,
                                   account_id=temp_user.id)
        logging.info('Adding review to database from user: ' + current_user.username)
        db.session.add(form_entry)
        db.session.commit()
        logging.info('Added review to database from user: ' + current_user.username)
        return redirect('/')
    return render_template('review.html', Form=form)

from flask_login import UserMixin

from app import db


class Review(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    rating = db.Column(db.Integer)
    content = db.Column(db.String(200))
    account_id = db.Column(db.Integer, db.ForeignKey('account.id'))
    book_id = db.Column(db.Integer, db.ForeignKey('book.id'))

    def __repr__(self):
        return '{}{}{}{}{}'.format(self.id, self.rating, self.content, self.account_id, self.book_id)


class Account(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(32), nullable=False)
    password = db.Column(db.String(32), nullable=False)
    date_of_birth = db.Column(db.Date)
    reviews = db.relationship('Review', backref='account', lazy='dynamic')

    def __repr__(self):
        return '{}{}{}{}'.format(self.id, self.username, self.password, self.date_of_birth)


class Book(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64))
    genre = db.Column(db.String(32))
    reviews = db.relationship('Review', backref='book', lazy='dynamic')

    def __repr__(self):
        return '{}{}{}'.format(self.id, self.name, self.genre)

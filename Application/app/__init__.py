from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_admin import Admin
from flask_login import LoginManager, UserMixin
import logging

logging.basicConfig(level=logging.DEBUG, encoding='utf-8')
app = Flask(__name__)
app.config.from_object('config')
db = SQLAlchemy(app)

login_manager = LoginManager()
login_manager.init_app(app)

migrate = Migrate(app, db)
admin = Admin(app, template_mode='bootstrap3')

from app import views, models

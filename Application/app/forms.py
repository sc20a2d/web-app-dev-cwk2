from flask_wtf import Form
from wtforms import BooleanField, TextAreaField, StringField, DateField, IntegerField, PasswordField, SubmitField, \
    SelectField
from wtforms.validators import DataRequired, Length, NumberRange


class CreateReview(Form):
    book_title = SelectField('Book Title', choices=[], validators=[DataRequired()])
    rating = IntegerField('Rating', validators=[DataRequired(), NumberRange(1, 5)])
    review_content = TextAreaField('Content', validators=[DataRequired(), Length(max=200)])


class CreateAccount(Form):
    username = StringField('Username', validators=[DataRequired(), Length(min=5)])
    password = PasswordField('Password', validators=[DataRequired(), Length(min=8)])
    date_of_birth = DateField('Deadline Date', validators=[DataRequired()], format='%Y-%m-%d')


class ChangePassword(Form):
    old_password = PasswordField('Password', validators=[DataRequired(), Length(min=8)])
    password = PasswordField('Password', validators=[DataRequired(), Length(min=8)])


class LogIn(Form):
    username = StringField('Username', validators=[DataRequired(), Length(min=8)])
    password = PasswordField('Password', validators=[DataRequired(), Length(min=8)])
    stayLoggedIn = BooleanField('Logged', validators=[])
